#!/bin/bash

sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y

# create an instructor user
sudo adduser --disabled-password --gecos "" --ingroup admin instructor 

# create keys for the instructor account and add authorized keys
sudo mkdir /home/instructor/.ssh/
sudo ssh-keygen -f /home/instructor/.ssh/id_rsa -t rsa -b 4096 -N ''
sudo sh -c 'echo " " >> /home/instructor/.ssh/authorized_keys'

sudo sh -c 'echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDgDXwEXzlsE0VsAmWcckvknP2m6Ir5Dk7Y2vuIvPLcKRc8eZEgXP6vvQr0Bu+AfGLZCaM5cjzO5eYp6KCWRRgf5SwyeQ1Rs7ho0tdzesoEAXjThHeap9eRxyq3QXD1nfYJCkMhelNlw679X+QCBv2JGdJt8eNYc9QWIiq7FB3jdnyNKZu9hTBYkqXPbJjEMk3Qz4pXIa+bEMcyl9S4FQbYcjNErrh9sIGyvh8gYsb7/5olW+9PXIv5ITog0i3x3hhNn99+gxe7DiZjH+3vnlyhbvVX6GCAcCNWqMxkpZDUYZSkmGNUbWw37dD8nwm7dTX2l0oSeoUyxRTE/43qq2aej3MjGKyhz+K56qJSDzWtfvD8NaHuXXPQ6tXC8nuBQNmZtOP5PAMqBPjs7SJphy97xtffaGJ6Vh13jdq86wr1g8vkwnZThgOT4x9GULR7yXweT6UqgmIOpdj8e6H7uOeO8dttaY3V9kKNFcNnmKI5/IK9x0xN0mysb5Gi7vMK9J/Otaf+CGK690jpDxnx3w33Y+UYpo67Xy/0HtixyYBsnaWNp9fhNFxM2ahOKDOCaalE6CuUeqhUNPeP+1r0TZ6aIPb5sTGRxZXkJKp2HlPYBKRNlcu8K9StdPfuDcEpKcdpTcrf/91bP49Dwd8OkQa9A45LnA45Gt8eXZlg6P8cRQ== mpm@vpn-campus-152-3-71-27.ssl.vpn.duke.edu" >> /home/instructor/.ssh/authorized_keys'

sudo sh -c 'echo " " >> /home/instructor/.ssh/authorized_keys'
sudo chown -R instructor:admin /home/instructor/.ssh

# enable passwordless sudo for instructor account
sudo sh -c 'echo "instructor  ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/20_instructor'


# ========= start Docker install
sudo apt-get update
sudo apt-get install -y \
    linux-image-extra-$(uname -r) \
    linux-image-extra-virtual
	
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
sudo apt-get update
sudo apt-get install -y docker-ce 
# ========= end Docker install

