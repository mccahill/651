First cut at a setup script for the 651 course.

setup.sh is intended to be run on an Ubuntu 16.04 VM to 
- create a user named 'instructor'
- set up public key-based authentication for access to the user
- give instructor sudo rights
- install the Docker so that student can run containers on the VM


*** assumptions:

- we assume that this is run ona a VM provisioned by vcm.duke.edu
- the student account was already set up with then VM was provisioned
- the instructor account should have the public keys for the instructor and TA
  so they can get access to the student's VM if necessary
  
*** still to be done

- replace mccahill's public key with the instructor and TA's keys
- find out what containers should be installed already (if any)
- ??? add a registration service so that the instructor knows which VM's are running this setup
